#include <complex.h>
#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdint.h>
#include <unistd.h>

#define SHM_SIZE 4096
#define PACKET_LEN 1024

volatile sig_atomic_t got_sigint = 0;
static uint32_t lfsr = 0x7FFFFFFF;
sem_t *sem_id;
char SHMEM_SEM[] = "shmem_sem";

// SIGINT handler
void sigint_han(int sig) {
  printf("SIGINT\n");
  got_sigint = 1;
}

// Shift LFSR one bit
uint32_t lfsr_shift() {
  uint32_t r31 = (lfsr & 0x40000000) >> 30;
  uint32_t r28 = (lfsr & 0x08000000) >> 27;

  uint32_t bit_out = r31 ^ r28;

  lfsr = lfsr << 1;
  if (bit_out) {
    lfsr++;
  }

  return bit_out;
}

// PRBS31 data generator, 32 bits at a time
uint32_t prbs31() {
  uint32_t output;
  for (uint32_t i = 0; i < 31; i++) {
    output += lfsr_shift();
    output << 1;
  }
  output += lfsr_shift();
  return output;
}

// Convert two bits to QPSK symbol
float complex two_bits_to_qpsk(uint32_t bits) {
  switch(bits)
  {
    case 0:
      return 1.0 + 1.0 * I;
    case 1:
      return 1.0 - 1.0 * I;
    case 2:
      return -1.0 - 1.0 * I;
    default:
      return -1.0 + 1.0 * I;
  }
}

// Bits to QPSK symbols
void packet_to_symbols(uint32_t *data, float complex *output) {
  uint32_t bits;
  for (uint32_t i = 0; i < PACKET_LEN; i++) {
    for (uint32_t j = 0; j < 16; j++) {
      bits = (data[i] >> (2 * j)) & 3;
      output[16 * i + j] = two_bits_to_qpsk(bits);
    }
  }
}

// Data Producer
void data_producer(uint32_t *shmem) {

  uint32_t *packet = malloc(sizeof(uint32_t) * PACKET_LEN);

  while (!got_sigint) {
    // Fill up a data packet 4 KB
    for (uint32_t i = 0; i < PACKET_LEN; i++) {
      packet[i] = prbs31();
    }
    sem_wait(sem_id);
    memcpy(shmem, packet, sizeof(uint32_t) * PACKET_LEN);
    sem_post(sem_id);
  }

  printf("Producer cleaning up...\n");
  free(packet);

  if (shmdt(shmem) == -1) {
    perror("shmdt");
    exit(1);
  }

  sem_close(sem_id);

  printf("Producer done\n");
}

// Data consumer
void data_consumer(uint32_t *shmem) {

  uint32_t *packet = malloc(sizeof(uint32_t) * PACKET_LEN);
  float complex *symbols = malloc(sizeof(float complex) * PACKET_LEN * 16);

  printf("0x%x\n", &packet);
  printf("0x%x\n", &symbols);

  while (!got_sigint) {
    sem_wait(sem_id);
    memcpy(packet, shmem, sizeof(uint32_t) * PACKET_LEN);
    sem_post(sem_id);
    packet_to_symbols(packet, symbols);
    printf("%d + j%d\n", creal(symbols[0]), cimag(symbols[0]));
  }

  printf("Consumer cleaning up...\n");
  printf("0x%x\n", &packet);
  printf("0x%x\n", &symbols);
  free(packet);
  free(symbols);

  sem_close(sem_id);

  printf("Freeing done\n");
  if (shmdt(shmem) == -1) {
    perror("shmdt");
    exit(1);
  }
  printf("Consumer done\n");
}

int main(int argc, char *argv[]) {

  // SIGING handling
  struct sigaction sa;
  got_sigint = 0;

  sa.sa_handler = sigint_han;
  sa.sa_flags = 0;
  sigemptyset(&sa.sa_mask);

  if (sigaction(SIGINT, &sa, NULL) == -1) {
    perror("sigaction");
    exit(1);
  }

  // Shared memory creation and pointer acquisition
  key_t key;
  int shmid;
  uint32_t *shmem;
  int mode;

  // Make key
  if ((key = ftok("test.c", 'R')) == -1) {
    perror("ftok");
    exit(1);
  }

  // Connect to the shared memory segment
  if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1) {
    perror("shmget");
    exit(1);
  }

  // Attach to the segment to get a ptr to it
  shmem = shmat(shmid, (void *)0, 0);
  if (shmem == (uint32_t *)(-1)) {
    perror("shmat");
    exit(1);
  }

  // Semaphore setup
  sem_id = sem_open(SHMEM_SEM, O_CREAT, 0644, 1);
  if (sem_id == SEM_FAILED) {
    perror("sem_init");
    sem_unlink(SHMEM_SEM);
    exit(1);
  }

  if (!fork()) {
    // Child - producer
    data_producer(shmem);
    exit(0);
  } else {
    // Parent - consumer
    data_consumer(shmem);
    wait(NULL);
    printf("Cleaned up child\n");
  }

  sem_unlink(SHMEM_SEM);
  if ((shmctl(shmid, IPC_RMID, 0)) == -1) {
    perror("shmctl");
    exit(1);
  }

  return 0;
}
