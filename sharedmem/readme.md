Shared Memory Segments
======================

Pulled directly from 
[here](http://beej.us/guide/bgipc/output/html/multipage/shm.html).

Run shmdemo without any arguments to read what is currently in the segment, 
run it with a string argument to write that string to the segment.  When done 
playing around be sure to run cleanup or `ipcrm -m <shmid>` to avoid memory 
leaks!

shmid can be obtained by checking `ipcs`.
