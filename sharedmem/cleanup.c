#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define SHM_SIZE 1024

int main(int argc, char *argv[])
{
  key_t key;
  int shmid, shmstat;

  if (argc > 2)
  {
    fprintf(stderr, "usage: shmdemo [data_to_write]\n");
    exit(1);
  }

  if ((key = ftok("shmdemo.c", 'R')) == -1)
  {
    perror("ftok");
    exit(1);
  }

  if ((shmid = shmget(key, SHM_SIZE, 0644 | IPC_CREAT)) == -1)
  {
    perror("shmget");
    exit(1);
  }

  if ((shmstat = shmctl(shmid, IPC_RMID, NULL)) == -1)
  {
    perror("shmctl");
    exit(1);
  }
  return 0;
}
