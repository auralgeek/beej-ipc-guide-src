Fork Primer
===========

Obtained directly from 
[here](http://beej.us/guide/bgipc/output/html/multipage/fork.html).

Run the demo with `forkdemo`.  The process starts a child, the parent and child
print their PIDs, and then they both exit.
