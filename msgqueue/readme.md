Message Queues
==============

Obtained directly from 
[here](http://beej.us/guide/bgipc/output/html/multipage/mq.html).

This is a simple producer/consumer message queue demo, with the producer being 
the kirk app, and the consumer being the spock app.  Several messages can be 
queued via the kirk app prior to starting the spock app, but the queue is 
destroyed when the kirk app is exited, so it will need to be running any time 
you want to start the spock app.
