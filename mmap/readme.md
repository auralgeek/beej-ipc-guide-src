Memory Mapped Files
===================

Obtained directly from 
[here](http://beej.us/guide/bgipc/output/html/multipage/mmap.html)

Run `mmapdemo <offset>` to map the mmapdemo.c source code directly into memory 
and index it to the argument offset.
