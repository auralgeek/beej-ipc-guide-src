Unix Sockets
============

Obtained directly from 
[here](http://beej.us/guide/bgipc/output/html/multipage/unixsock.html).

Two sets of examples here, a client/server framework with echos and echoc, and 
a full duplex example of socket pair with spair.  To run the client/server 
example start up the server (echos), then the client (echoc), and type in some 
string to be bounced off the server into the client app.  The socket pair demo
can be run by itself.
