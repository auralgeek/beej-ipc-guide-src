Beej's Guide to Unix IPC
========================

This is some of the examples presented in
[Beej's Guide to Unix IPC](http://beej.us/guide/bgipc/output/html/multipage/index.html),
which is an excellent little tutorial on IPC in Unix based systems using C.

Another valuable resource with more in depth details is the
[Linux Programmers's Guide](http://tldp.org/LDP/lpg/node7.html).
